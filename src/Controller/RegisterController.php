<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/register", name="register")
     */
    public function index(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        $user = new User();

        //Create form
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $user = $form->getData();

            //Hash password
            $password = $hasher->hashPassword($user, $user->getPassword());

//            dd($password);

            $user->setPassword($password);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

//            $doctrine = $this->getDoctrine()->getManager();
//            $doctrine->persist($user);
//            $doctrine->flush();



//            dd($user);
        }



        return $this->render('register/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
